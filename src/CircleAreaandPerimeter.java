
public class CircleAreaandPerimeter {
	

	public static void main(String[] args) {
		
		int r = 5;
		
		System.out.println("Area of the circle is " + 3.14 * r * r);
		System.out.println("Perimeter of the circle is " +  3.14 * 2 * r);
		System.out.println();
		
		// Or we can do the following way as well.
		
		double A = 3.14 * r*r;
		double P = 2 * 3.14 * r;
		
		System.out.println("Area of the circle is " + A);
		System.out.println("Perimeter of the circle is " + P);
		
	}

			
}
