public class Fibonacci {


	public static void main(String[] args) {
    	
    	int i = 0, count = 10, n1 = 0, n2 = 1;
    	
    	System.out.println("Fibonacci Series for " + count + ":" );
    	
    	while (i<=count) {
    		int sum = n1 + n2;
    		n1 = n2;
    		n2  = sum;
    		i++;
    		
    		} System.out.println(n1 + "+" );
    	}
    
}

/*public class Fibonacci {

    public static void main(String[] args) {

        int i = 1, n = 10, t1 = 0, t2 = 1;
        System.out.print("First " + n + " terms: ");

        while (i <= n)
        {
            System.out.print(t1 + " + ");

            int sum = t1 + t2;
            t1 = t2;
            t2 = sum;

            i++;
        }
    }
}*/