
public class VariableSwap {

	public static void main(String[] args) {
		
		int a = 5;
		int b = 7;		
		int c;
		
		//Print before swapping
		System.out.println("value of a is " + a  + " and" +  " value of b is " + b);
				
		 c = a;
		 a = b;
		 b = c;
		
		//Print after swapping
			System.out.println("value of a is " + a  + " and" +  " value of b is " + b);
		
	}

}
