
public class LocalVariables {

	public static void main(String[] args) {
		
		int x;  //x is not initialized. But it is always recommended to initialize local variables at least with default values.
		if (args.length > 0)
		{
			final int y =10;
			System.out.println(y);

		}
			else 
			{
				x=20;
			}

		//System.out.println(y);

	}

}
 