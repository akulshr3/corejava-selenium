
public class SwapNumbersWithoutThird {

	public static void main(String[] args) {
		
		int a = 5;
		int b = 7;
		
		System.out.println("value of a is " + a  + " and" +  " value of b is " + b);
		System.out.println("----------------------");
		
		//Values swapped
		System.out.println("After swapping: ");
		System.out.println("value of a is " + b + " and" + " value of b is " + a);
	}

}
