
public class SumOfTwoNumbers {
	
	static int a = 10; // Instance variable, as it is defined directly inside a class but outside of any method.
	int b = 20; // Instance variable, as it is defined directly inside a class but outside of any method.
	
	public static void main(String[] args) {
		/*int a = 10;
		int b = 20;		
		System.out.println(a+b); */	
		summation (); //Calling Summation method.
		
		SumOfTwoNumbers x = new SumOfTwoNumbers ();
		System.out.println(a);  // a is declared as static variable, so can be directly accessed within static main method.
		System.out.println(SumOfTwoNumbers.a); // a is declared as static variable, so can be accessed using class name. This is recommended.
		System.out.println(x.a);  // a is declared as static variable, so can be accessed using class object reference.
		
			}
	
	static void summation () {
	
		SumOfTwoNumbers x = new SumOfTwoNumbers (); //Creating object x which is part of class SumOfTwoNumers.
		SumOfTwoNumbers y = new SumOfTwoNumbers(); //Creating object y which is part of class SumOfTwoNumbers.
		
		int sum =  SumOfTwoNumbers.a + y.b;  //a is referenced using class name as a is declared as static variable.
		System.out.println(sum);
	}
				
}

