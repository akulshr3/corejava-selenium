class Person {
	
	String name;
	int age;
}

class Animal {
	
	String breed;
	String color;
	int weight;
	String name;
	
	public Animal (String breed) {
		this.breed = breed;
		System.out.println("Animal created");
		
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

//------------------------------------------------------

public class ClassesAndObjects {	
	
	public static void main(String[] args) {
		
		Person john = new Person ();
		Person bob = new Person ();

		john.name = "John";
		john.age = 20;
		
		bob.name = "Bob";
		bob.age = 25;
		
		System.out.println("John is "  + john.age + " years old");
		System.out.println("Bob is "  + bob.age + " years old");

		Animal simba = new Animal("Lab");
		
		simba.setBreed("Lab");
		simba.setColor("Brown");
		simba.setWeight(70);
		simba.setName("Simba");
		
		System.out.println("------------------------------------");
	
		System.out.println(simba.getBreed () + " "  + simba.getColor ());
			
		}
	} 

